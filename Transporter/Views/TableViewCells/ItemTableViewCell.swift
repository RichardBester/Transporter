//
//  ItemTableViewCell.swift
//  Transporter
//
//  Created by Sauron Black on 5/6/15.
//  Copyright (c) 2015 Totenkopf. All rights reserved.
//

import UIKit

class ItemTableViewCell: UITableViewCell {
    
    @IBOutlet weak var itemImageView: UIImageView!
    @IBOutlet weak var itemTitleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        selectedBackgroundView = UIView()
        selectedBackgroundView?.backgroundColor = UIColor(white: 0.0, alpha: 0.4)
    }
}
